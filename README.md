# Documentation
* [Reserved API](#reserved-api)

## Reserved API
### GET /reserved
Returns all Reserved instance.

Example response:
```json
[
    {
        "table": 2,
        "client": "Test",
        "_id": "5dd31f81d55c9a23c484041c",
        "date": "2019-11-29T22:00:00.000Z",
        "phone": "0676767676",
        "__v": 0
    }
]
```

### POST /reserved
Adds to Reserved instance in a database.

|Param|Type|
|--|--|
|date|`string`|
|table|`number`|
|client|`string`|
|phone|`string`|

Example request:
```json
{
    "table": 2,
    "client": "Test",
    "date": "2019-11-29T22:00:00.000Z",
    "phone": "0676767676"
}
```
Example response:
```json
{
    "message": "Successfully created!"
}
```

### GET /reserved/:id
Returns a Reserved instance by id.

Example response:
```json
{
    "table": 2,
    "client": "Test",
    "_id": "5dd31f81d55c9a23c484041c",
    "date": "2019-11-29T22:00:00.000Z",
    "phone": "0676767676",
    "__v": 0
}
```
### PUT /reserved/:id
Updates to Reserved instance in a database.

|Param|Type|
|--|--|
|date|`string`|
|table|`number`|
|client|`string`|
|phone|`string`|

Example request:
```json
{
    "table": 2,
    "client": "Test",
    "date": "2019-11-29T22:00:00.000Z",
    "phone": "0676767676"
}
```
Example response:
```json
{
    "message": "Successfully updated!"
}
```
### DELETE /reserved/:id
Delete a Reserved instance by id.

Example response:
```json
{
    "message": "Successfully deleted!"
}
```
